<?php
namespace SFM;

use SFM\Cache\CacheKeyGenerator;
use SFM\Cache\CacheKeyProvider;
use SFM\Cache\CacheProvider;
use SFM\Database\DatabaseProvider;
use SFM\IdentityMap\IdentityMap;
use SFM\Mapper\AggregateBuilder;
use SFM\Mapper\AggregateManager;
use Zend\Db\Sql\Predicate\Predicate;
use Zend\Db\Sql\Sql;

/**
 * Abstract class for Data Mapping
 */
abstract class Mapper
{
    /**
     * @var Manager
     */
    protected $manager;

    /**
     * @var DatabaseProvider
     */
    protected $database;

    /**
     * @var CacheKeyProvider
     */
    protected $cacheKeyProvider;

    /**
     * @param Manager $manager
     * @param DatabaseProvider $database
     * @param IdentityMap $identityMap
     * @param CacheProvider $cache
     * @param CacheKeyProvider $cacheKeyProvider
     */
    public function __construct(Manager $manager, DatabaseProvider $database, IdentityMap $identityMap, CacheProvider $cache, CacheKeyProvider $cacheKeyProvider)
    {
        $this->sql = new Sql($database->getAdapter());
        $this->manager = $manager;
        $this->database = $database;
        $this->cache = $cache;
        $this->identityMap = $identityMap;
        $this->cacheKeyProvider = $cacheKeyProvider;
    }

    /**
     * @return string
     */
    public function getAggregateClassName()
    {
        return str_replace('Mapper', 'Aggregate', get_class($this));
    }

    /**
     * @return string
     */
    public function getEntityClassname()
    {
        return str_replace('Mapper', 'Entity', get_class($this));
    }

    /**
     * Returns name of the db table
     * @return mixed
     */
    public abstract function getTableName();

    /**
     * Returns name of field that is unique to every Entity.
     * Entity has no ability to change it
     *
     * @return string
     */
    public function getIdField()
    {
        return 'id';
    }

    /**
     * Is table id autoincrement
     * @return bool
     */
    public function isIdAutoIncrement()
    {
        return true;
    }

    /**
     * Returns list of fields that is unique to every Entity.
     * Entity has no ability to change it
     *
     * @return array
     */
    public function getUniqueFields()
    {
        return [];
    }

    public function getEntityById($id)
    {
        if (is_numeric($id) && $id > 0) {
            //First looks to IndentityMap
            $entity = $this->identityMap->getEntity($this->getEntityClassname(), $id);
            if( null !== $entity ) {
                return $entity;
            }
            //Second looks to Cache
            $cacheKey = (new CacheKeyGenerator())->getEntityCacheKeyById($id, $this->getEntityClassname());
            $entity = $this->cache->get($cacheKey);
            if (null !== $entity) {
                return $entity;
            }
        } else {
            throw new BaseException("Illegal argument type given; id: ".$id);
        }
        //Then looks to DB. Check that Entity exists
        $entity = $this->getEntityFromDB(array($this->getIdField()=>$id));

        if (null !== $entity) {
            //Store Entity in Cache. Since now we store only cacheable entities
            $this->saveCached($entity);
        }
        return $entity;

    }

    /**
     * Get Entity by unique fields.
     * Note: You can use only two ways to guarantee single object is received:
     *  - getEntityById
     *  - getEntityByUniqueFields
     * @see getEntityById
     * @param $params
     * @return Entity|null
     */
    public function getEntityByUniqueFields(array $params)
    {
        if ($this->getUniqueFields() && ($params = $this->getOneUniqueFromParams($params)) ) {
            $cacheKey = (new CacheKeyGenerator())->getEntityCacheKeyByUniqueVals($this->getUniqueVals($params), $this->getEntityClassname());
            $entityId = $this->cache->getRaw($cacheKey);
            if( null !== $entityId ) {
                return $this->getEntityById( $entityId );
            }
        } else {
            throw new BaseException("Unique fields aren't set");
        }

        $entity = $this->getEntityFromDB($params);

        //aazon: now we check either Entity is cacheable
        if (null !== $entity) {
            //to prevent unique fields mapping to empty cache object
            if (null === $this->cache->get($this->cacheKeyProvider->getCacheKey($entity))) {
                $this->saveCached($entity);
            }
            $uniqueKey = array_keys($params);
            $this->createUniqueFieldsCache($entity, $uniqueKey);
        }

        return $entity;
    }

    /**
     * Returns Entity object by prototype $proto
     * @param array $proto
     * @return Entity must be overrided in children
     */
    public function createEntity(array $proto)
    {
        $className = $this->getEntityClassname();
        if(array_key_exists($this->getIdField(), $proto)) {
            $entity = $this->identityMap->getEntity($className, $proto[$this->getIdField()]);
        } else {
            $entity = null;
        }
        if ($entity === null) {
            $entity = new $className($proto, $this->getIdField(), $this->manager);
            $this->identityMap->addEntity($entity);
        }
        return $entity;
    }

    /**
     * Updates Entity in Database
     * Do not call this method directly! Use Entity::update
     * @todo Check values from $params to be in datamap
     *
     * @param string[] $updates
     * @param Entity $entity
     * @return bool
     */
    public function updateEntity(array $updates, Entity $entity)
    {
        $oldEntity = clone $entity;

        // continue if there is no work
        if (empty($updates)) {
            return true;
        }

        // prevent changing id
        unset($updates[$this->getIdField()]);

        // update statement
        $update = $this->sql->update($this->getTableName())
            ->set($updates)
            ->where([$this->getIdField() => $entity->getId()]);

        $statement = $this->sql->prepareStatementForSqlObject($update);

        $state = $this->database->query()->byStatement($statement)->count();

        // update memory
        foreach ($updates as $key => $value) {
            if (array_key_exists($key, $entity->getPrototype())) {
                $entity->setPrototype($key, $value);
            }
        }
        $this->updateUniqueFields($entity, $oldEntity);

        // update identity map
        $this->identityMap->addEntity($entity);

        // update cache
        $this->saveCached($entity);

        return $state;
    }

    /**
     * Search in $newEntity new values of unique fields and update key if needed
     *
     * @param Entity $newEntity
     * @param entity $oldEntity
     * @return void
     */
    public function updateUniqueFields(Entity $newEntity, Entity $oldEntity )
    {
        $changedUniqueKeys = array();

        if($this->getUniqueFields()) {
            foreach ($this->getUniqueFields() as $uniqueKey) {
                foreach ( $uniqueKey as $field ) {
                    if( $oldEntity->getPrototype($field) != $newEntity->getPrototype($field)) {
                        $changedUniqueKeys[] = $uniqueKey;
                    }
                }
            }
            if( sizeof($changedUniqueKeys) != 0 ) {
                foreach ($changedUniqueKeys as $key) {

                    $cacheKey = (new CacheKeyGenerator())->getEntityCacheKeyByUniqueFields($oldEntity, $key, $this->getEntityClassname());
                    $this->cache->delete($cacheKey);
                    $this->createUniqueFieldsCache( $newEntity, $key );
                }
            }
        }
    }

    /**
     * Deletes Entity from Database
     *
     * @param Entity $entity
     * @return bool
     */
    public function deleteEntity(Entity $entity)
    {
        // delete from identity map
        $this->identityMap->deleteEntity($entity);

        // delete from cache
        $this->cache->deleteEntity($entity);
        if ($this->getUniqueFields()) {
             foreach ($this->getUniqueFields() as $uniqueKey) {
                 $cacheKey = (new CacheKeyGenerator())->getEntityCacheKeyByUniqueFields($entity, $uniqueKey, $this->getEntityClassname());
                 $this->cache->delete($cacheKey);
             }
        }

        // statement
        $delete = $this->sql->delete($this->getTableName())
            ->where([$this->getIdField() => $entity->getId()]);

        $statement = $this->sql->prepareStatementForSqlObject($delete);
        $result = $this->database->query()->byStatement($statement);

        return $result->count();
    }

    /**
     * Executes insert SQL query and returns Entity

     * @param $prototype
     * @return Entity
     */
    public function insertEntity($prototype)
    {
        if ($this->isIdAutoIncrement()) {
            unset($prototype[$this->getIdField()]);
        }

        $insert = $this->sql->insert($this->getTableName())
            ->values($prototype);

        $statement = $this->sql->prepareStatementForSqlObject($insert);
        $result = $this->database->query()->byStatement($statement);

        if ($this->isIdAutoIncrement()) {
            $id = $result->getGeneratedValue();
        } else {
            $id = $prototype[$this->getIdField()];
        }

        return $this->getEntityById($id);
    }

    /**
     * @TODO First look in indentityMap then cache ...
     * Load entities by id.
     * First get all from cache, then from DB
     * @return array of Entities
     */
    public function getMultiEntitiesByIds( array $entityId )
    {
        if( sizeof($entityId) == 0 || null == $entityId) {
            return array();
        }

        //from identity map
        $cachedIdentityMapVals = $this->identityMap->getEntityMulti($this->getEntityClassname(), $entityId);
        $entityId = array_diff($entityId,array_keys($cachedIdentityMapVals));

        $rawKeys = array();
        foreach ($entityId as $item) {
            $rawKeys[] = (new CacheKeyGenerator())->getEntityCacheKeyById($item, $this->getEntityClassname());
        }

        $memcachedVals = $this->cache->getMulti($rawKeys);
        $cachedVals = $cachedIdentityMapVals;
        if($memcachedVals){
            $cachedVals = array_merge($cachedVals,$memcachedVals);
        }

        $foundedId = array();
        if( null != $cachedVals ) {
            foreach ($cachedVals as $item) {
                $foundedId[] = $item->getId();
            }
        } else {
            $cachedVals = array();
        }
        $notFoundedId = array_diff($entityId, $foundedId);
        $dbVals = $this->loadEntitiesFromDbByIds($notFoundedId);
        if( sizeof($dbVals) > 0 ) {
            $this->cache->setMulti($dbVals);
        }
        $result = array_merge($cachedVals, $dbVals);
        return $result;
    }

    protected function loadEntitiesFromDbByIds(array $entityIds)
    {
        $entities = [];
        if (sizeof($entityIds) > 0) {

            $select = $this->sql->select($this->getTableName())
                ->columns(['*'])
                ->where([
                    (new Predicate())->in($this->getIdField(), $entityIds)
                ]);

            $statement = $this->sql->prepareStatementForSqlObject($select);
            $data = $this->database->query()->byStatement($statement)->toArray();

            foreach ($data as $row) {
                $entity = $this->createEntity($row);
                $entities[$entity->getId()] = $entity;
            }
        }

        return $entities;
    }

   /**
     * @param array $params
     * @return Entity
     */
    protected function getEntityFromDB( array $params )
    {
        $data = $this->fetchArrayFromDB($params);
        if (count($data) > 1) {
            throw new BaseException('More than 1 row in result set');
        } elseif (count($data) == 0) {
            return null;
        }

        //So, count($data) == 1, it is our case :-)
        $proto = array_shift($data);
        return $this->createEntity($proto);
    }

    /**
     * Returns result set by means of which Entity will be generated
     *
     * @param array $params
     * @return Array
     */
    protected function fetchArrayFromDB(array $params)
    {
        $select = $this->sql->select($this->getTableName())
            ->columns(['*'])
            ->where($params);

        $statement = $this->sql->prepareStatementForSqlObject($select);
        return $this->database->query()->byStatement($statement)->toArray();
    }

    /**
     * Stores in Cache
     *
     * @param Business $object Entity object
     */
    protected function saveCached(Business $object)
    {
        $cacheKey = $this->cacheKeyProvider->getCacheKey($object);

        if (null !== $cacheKey) {

            //reset only for entities
            if ($object instanceof Entity) {
                $this->cache->deleteEntity($object);
            }
            $this->cache->set($object);
        }
    }

    /**
     * @param array $uniqueKey
     * @param Entity $entity
     */
    protected function createUniqueFieldsCache(Entity $entity, array $uniqueKey)
    {
        if($this->getUniqueFields()) {
            $key = (new CacheKeyGenerator())->getEntityCacheKeyByUniqueFields($entity, $uniqueKey, $this->getEntityClassname());
            $this->cache->setRaw($key, $entity->getId());
        }
    }

    /**
     * Check if array contains all fields of any unique keys
     * and return first matched key or false if no key founded
     *
     * @param array $params
     * @return array|false
     */
    protected function getOneUniqueFromParams( array $params )
    {
        $result = false;

        if(!$this->getUniqueFields()) {
            return false;
        }

        foreach ($this->getUniqueFields() as $uniqueKey) {
            $match = array();
            foreach ($params as $key => $val) {
                if(in_array($key, $uniqueKey)) {
                    $match[$key] = $val;
                }
            }
            if( sizeof($uniqueKey) === sizeof($match) ) {
                $result = $match;
                break;
            }
        }
        return $result;
    }

    /**
     * @param array $params
     * @return array
     */
    protected function getUniqueVals( array $params )
    {
        if(!$this->getUniqueFields()) {
            return array();
        }
        $result = array();
        foreach ($params as $field => $val) {
            $result[] = $params[$field];
        }
        return $result;
    }

    /**
     * Get or create aggregate
     *
     * @param string|null $cacheKey
     * @return AggregateBuilder
     */
    public function getAggregate($cacheKey = null)
    {
        return new AggregateBuilder($this->database, $this->cache, $this, $cacheKey);
    }

    /**
     * Modify existing aggregate
     *
     * @param Aggregate $aggregate
     * @return AggregateManager
     */
    public function modifyAggregate(Aggregate $aggregate)
    {
        return new AggregateManager($this->cache, $this, $aggregate);
    }
}