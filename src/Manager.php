<?php
namespace SFM;

use SFM\Cache\CacheKeyProvider;
use SFM\Cache\CacheKeyStrategy\AggregateCacheKeyStrategy;
use SFM\Cache\CacheKeyStrategy\BusinessCacheKeyStrategy;
use SFM\Cache\CacheKeyStrategy\EntityCacheKeyStrategy;
use SFM\Database\Config;
use SFM\Database\DatabaseProvider;
use SFM\Cache\CacheProvider;
use SFM\Cache\Session;
use SFM\IdentityMap\IdentityMap;
use SFM\IdentityMap\IdentityMapStorage;
use SFM\Transaction\TransactionAggregator;
use SFM\Value\ValueStorage;
use Zend\Db\Adapter\Adapter;
use Pimple\Container;

class Manager extends Container
{
    protected static $instance;

    /**
     * @return Manager
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            $container = new Manager();
            $container->reset();

            self::$instance = $container;
        }

        return self::$instance;
    }

    /**
     * @return $this
     */
    public function reset()
    {
        $this['adapter'] = function() {

            if (!$this->getConfigDb() instanceof Config) {
                throw new BaseException("DatabaseProvider is not configured");
            }

            $adapter = new Adapter(array(
                'driver' => $this->getConfigDb()->getDriver(),
                'database' => $this->getConfigDb()->getDb(),
                'username' => $this->getConfigDb()->getUser(),
                'password' => $this->getConfigDb()->getPass(),
                'hostname' => $this->getConfigDb()->getHost()
            ));

            if (is_array($this->getConfigDb()->getInitialQueries())) {
                foreach ($this->getConfigDb()->getInitialQueries() as $query) {
                    $adapter->query($query, []);
                }
            }

            return $adapter;
        };

        $this['value_storage'] = function () {
            return new ValueStorage($this->getCache());
        };

        $this['db'] = function () {
            return new DatabaseProvider($this['adapter']);
        };

        $this['cacheMemory'] = function () {
            $cache = new CacheProvider($this->getCacheKeyProvider(), $this);
            $cache->init($this['cache_config']);
            $cache->connect();

            return $cache;
        };

        $this['cacheSession'] = function () {
            return new Session($this->getCacheKeyProvider(), $this);
        };

        $this['identityMap'] = function () {
            return new IdentityMap(new IdentityMapStorage(), new IdentityMapStorage(), new IdentityMapStorage());
        };

        $this['transaction'] = function () {
            $transaction = new TransactionAggregator();
            foreach ($this['transaction_engines'] as $engine) {
                $transaction->registerTransactionEngine($engine);
            }

            return $transaction;
        };

        $this['transaction_engines'] = function () {
            return [
                $this->getDb(),
                $this->getCache()->getAdapter(),
                $this->getIdentityMap()
            ];
        };

        $this['mapper_factory'] = $this->protect(function($mapper) {
            $className = 'Mapper_' . $mapper;
            if (false === isset($this[$className])) {
                $this[$className] = new $className($this, $this->getDb(), $this->getIdentityMap(), $this->getCache(), $this->getCacheKeyProvider());
            }

            return $this[$className];
        });

        $this['cacheKeyProvider.Entity'] = function () {
            return new EntityCacheKeyStrategy;
        };

        $this['cacheKeyProvider'] = function () {
            $cacheKeyProvider = new CacheKeyProvider();
            $cacheKeyProvider->registerStrategy($this['cacheKeyProvider.Entity']);
            $cacheKeyProvider->registerStrategy(new AggregateCacheKeyStrategy($this['cacheKeyProvider.Entity']));
            $cacheKeyProvider->registerStrategy(new BusinessCacheKeyStrategy);
            return $cacheKeyProvider;
        };

        return $this;
    }

    /**
     * @return DatabaseProvider
     */
    public function getDb()
    {
        return $this['db'];
    }

    /**
     * @return Config
     */
    public function getConfigDb()
    {
        return $this['db_config'];
    }

    /**
     * @return CacheProvider
     */
    public function getCache()
    {
        return $this['cacheMemory'];
    }

    /**
     * @return Session
     */
    public function getCacheSession()
    {
        return $this['cacheSession'];
    }

    /**
     * @return IdentityMap
     */
    public function getIdentityMap()
    {
        return $this['identityMap'];
    }

    /**
     * @return TransactionAggregator
     */
    public function getTransaction()
    {
        return $this['transaction'];
    }

    /**
     * @return ValueStorage
     */
    public function getValueStorage()
    {
        return $this['value_storage'];
    }

    /**
     * @return CacheKeyProvider
     */
    public function getCacheKeyProvider()
    {
        return $this['cacheKeyProvider'];
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function offsetGet($id)
    {
        try {
            $value = parent::offsetGet($id);
        } catch (\InvalidArgumentException $e) {
            $pattern = 'Mapper_';
            if (substr($id, 0, strlen($pattern)) == $pattern) {
                $baseName = substr($id, strlen($pattern));
                $value = $this['mapper_factory']->__invoke($baseName);
            } else {
                throw $e;
            }
        }

        return $value;
    }
}