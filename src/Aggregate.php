<?php
namespace SFM;

abstract class Aggregate extends Business implements \Iterator, \Countable
{
    /**
     * Loaded entities
     * @var Entity[]
     */
    protected $entities = [];

    /**
     * Loaded entity ids
     * @var array
     */
    protected $loadedEntityIds = [];

    /**
     * Total entity ids
     * @var array
     */
    protected $entityIds = [];

    /**
     * Unique aggregate identifier
     * @var string|null
     */
    protected $cacheKey = null;

    /**
     * Is not synced with storage
     * @var bool
     */
    protected $dirty = false;

    /**
     * @param int[] $ids
     * @param string $idField
     * @param string|null $cacheKey
     * @throws BaseException
     */
    public function __construct(array $ids, $idField, $cacheKey = null)
    {
        $this->cacheKey = $cacheKey;
        $this->entities = [];
        $this->entityIds = $ids;
        $this->loadedEntityIds = [];
        $this->dirty = false;
    }

    /**
     * Get loaded entities
     * @return Entity[]
     */
    public function getEntities()
    {
        return $this->entities;
    }

    /**
     * Get ids of loaded entities
     * @return int[]
     */
    public function getLoadedEntityIds()
    {
        return $this->loadedEntityIds;
    }

    /**
     * Get ids of all entities
     * @return int[]
     */
    public function getEntityIds()
    {
        return $this->entityIds;
    }

    /**
     * Implementation of Iterator interface
     * @return bool
     */
    public function rewind()
    {
        return empty($this->loadedEntityIds) ? false : reset($this->loadedEntityIds);
    }

    /**
     * Implementation of Iterator interface
     * @return Entity|bool
     */
    public function next()
    {
        if (empty($this->entities)) {
            return false;
        }

        $nextEntityId = next($this->loadedEntityIds);
        if ($nextEntityId === false) {
            return false;
        }

        return $this->entities[$nextEntityId];
    }

    /**
     * Implementation of Iterator interface
     * @return string|bool
     */
    public function key()
    {
        return empty($this->loadedEntityIds) ? false : key($this->loadedEntityIds);
    }

    /**
     * Implementation of Iterator interface
     * @return Entity|bool
     */
    public function current()
    {
        if (empty($this->entities)) {
            return null;
        }

        $currentEntityId = current($this->loadedEntityIds);
        if ($currentEntityId === false) {
            return null;
        }

        return $this->entities[$currentEntityId];
    }

    /**
     * Implementation of Iterator interface
     * @return bool
     */
    public function valid()
    {
        return empty($this->loadedEntityIds) ? false : current($this->loadedEntityIds) !== false;
    }

    /**
     * Implementation of Countable interface
     * @return int
     */
    public function count()
    {
        return empty($this->loadedEntityIds) ? 0 : count($this->loadedEntityIds);
    }

    /**
     * Total number of elements in Aggregate (include loaded and not loaded objects)
     */
    public function totalCount()
    {
        return count($this->entityIds);
    }

    /**
     * @param Entity $entity
     */
    public function push(Entity $entity)
    {
        $this->dirty = true;

        if (false === in_array($entity->getId(), $this->entityIds)) {
            array_push($this->entityIds, $entity->getId());
        }

        if (false === in_array($entity->getId(), $this->loadedEntityIds)) {
            array_push($this->loadedEntityIds, $entity->getId());
        }

        $this->entities[$entity->getId()] = $entity;
    }

    /**
     * Get unique cache key
     * @return string
     */
    public function getCacheKey()
    {
        return $this->cacheKey;
    }

    /**
     * @return string[]
     */
    public function __sleep()
    {
        return ['entityIds', 'cacheKey'];
    }

    public function __wakeup()
    {
        $this->entities = [];
        $this->entityIds = [];
    }

    /**
     * Is all entity objects loaded
     * @return boolean
     */
    public function isLoaded()
    {
        return sizeof($this->loadedEntityIds) == sizeof($this->entityIds);
    }

    /**
     * Is aggregate dirty
     * @return bool
     */
    public function isDirty()
    {
        return $this->dirty;
    }

    /**
     * 
     * @param int $id
     * @return Entity
     */
    public function getEntityById($id)
    {
        if (isset($this->entities[$id])) {
            return $this->entities[$id];
        }

        return null;
    }
}