<?php
namespace SFM\Mapper;

use SFM\Aggregate;
use SFM\Cache\CacheProvider;
use SFM\Entity;
use SFM\Mapper;

class AggregateManager
{
    /**
     * @var Aggregate
     */
    protected $aggregate;

    /**
     * @var CacheProvider
     */
    protected $cache;

    /**
     * @var Mapper
     */
    protected $mapper;

    /**
     * @param CacheProvider $cache
     * @param Mapper $mapper
     * @param Aggregate $aggregate
     */
    public function __construct(CacheProvider $cache, Mapper $mapper, Aggregate $aggregate)
    {
        $this->aggregate = $aggregate;
        $this->mapper = $mapper;
        $this->cache = $cache;
    }

    /**
     * Push entity to aggregate
     * @param Entity $entity
     * @return Aggregate
     */
    public function push(Entity $entity)
    {
        $this->aggregate->push($entity);
        $this->cache->set($this->aggregate);

        return $this->aggregate;
    }

    /**
     * Load all aggregate entities
     * @return Aggregate
     */
    public function loadAll()
    {
        $this->loadByIds($this->aggregate->getEntityIds());
        return $this->aggregate;
    }

    /**
     * Load one page of aggregate entities
     * @param int $pageNum
     * @param int $perPage
     * @return Aggregate
     */
    public function loadPage($pageNum, $perPage)
    {
        $itpp = $perPage;
        --$pageNum;
        $ids = array_slice($this->aggregate->getEntityIds(), $pageNum * $itpp, $itpp);
        $this->loadByIds($ids);

        return $this->aggregate;
    }

    /**
     * Load specific set of aggregate entities
     * @param int[] $entityIds
     * @return Aggregate
     */
    public function loadByIds($entityIds)
    {
        $notLoaded = array_diff($entityIds, $this->aggregate->getLoadedEntityIds());
        $newEntities = $this->mapper->getMultiEntitiesByIds($notLoaded);

        $tmp = array();
        $num = sizeof($newEntities);
        for ($i = 0; $i < $num; $i++) {
            /** @var Entity $entity */
            $entity = $newEntities[$i];
            $tmp[$entity->getId()] = $i;
        }

        foreach ($notLoaded as $id) {
            $entity = isset($tmp[$id]) && isset($newEntities[$tmp[$id]]) ? $newEntities[$tmp[$id]] : null;
            if (isset($entity)) {
                $this->aggregate->push($entity);
            }
        }

        return $this->aggregate;
    }
}