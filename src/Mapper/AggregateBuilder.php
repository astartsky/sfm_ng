<?php
namespace SFM\Mapper;

use SFM\Aggregate;
use SFM\BaseException;
use SFM\Cache\CacheProvider;
use SFM\Database\DatabaseProvider;
use SFM\Mapper;
use Zend\Db\Adapter\Driver\StatementInterface;

class AggregateBuilder
{
    /** @var null|string */
    protected $cacheKey = null;

    /** @var Mapper */
    protected $mapper;

    /** @var DatabaseProvider */
    protected $database;

    /** @var CacheProvider */
    protected $cache;

    /**
     * @param DatabaseProvider $database
     * @param CacheProvider $cache
     * @param Mapper $mapper
     * @param string|null $cacheKey
     */
    public function __construct(DatabaseProvider $database, CacheProvider $cache, Mapper $mapper, $cacheKey = null)
    {
        $this->cacheKey = $cacheKey;
        $this->database = $database;
        $this->mapper = $mapper;
        $this->cache = $cache;
    }

    /**
     * Get aggregate
     * @param StatementInterface $statement
     * @return Aggregate
     */
    public function byStatement(StatementInterface $statement)
    {
        $aggregate = $this->byCacheKey();
        if (!$aggregate instanceof Aggregate) {
            $aggregate = $this->bySQL($statement->getSql(), $statement->getParameterContainer()->getNamedArray());
            $this->cache->set($aggregate);
        }

        return $aggregate;
    }

    /**
     * Get aggregate
     * @param string $sql
     * @param string[] $params
     * @return Aggregate
     */
    public function bySQL($sql, array $params = [])
    {
        $aggregate = $this->byCacheKey();
        if (!$aggregate instanceof Aggregate) {
            $aggregate = $this->create($this->database->query()->bySql($sql, $params)->toArray());
            $this->cache->set($aggregate);
        }

        return $aggregate;
    }

    /**
     * Get aggregate
     * @return Aggregate
     */
    protected function byCacheKey()
    {
        $aggregate = null;
        if ($this->cacheKey) {
            $aggregate = $this->cache->get($this->cacheKey);
        }
        return $aggregate;
    }

    /**
     * Get aggregate
     * @param int[] $ids
     * @return Aggregate
     */
    public function byIds(array $ids = [])
    {
        $aggregate = $this->byCacheKey();
        if (!$aggregate instanceof Aggregate) {
            $aggregate = $this->create($ids);
            $this->cache->set($aggregate);
        }

        return $aggregate;
    }

    /**
     * @param array[] $prototypes
     * @return Aggregate
     * @throws BaseException
     */
    protected function create(array $prototypes)
    {
        $ids = [];
        /** @var string[] $prototype */
        foreach ($prototypes as $prototype) {

            // prototype value is id
            if (false === is_array($prototype) && intval($prototype) > 0) {
                $ids[] = (int) $prototype;
            // prototype array contains id
            } else if (isset($prototype[$this->mapper->getIdField()])) {
                $ids[] = (int) $prototype[$this->mapper->getIdField()];
            } else {
                throw new BaseException('Entity prototype array does not contain `id`');
            }
        }

        /**
         * Create blank aggregate
         * @var Aggregate $aggregate
         */
        $aggregateClass = $this->mapper->getAggregateClassName();
        $aggregate = new $aggregateClass($ids, $this->mapper->getIdField(), $this->cacheKey);

        /** @var string[] $prototype */
        foreach ($prototypes as $prototype) {
            // prototype contains all fields, not just `id` field
            if (is_array($prototype) && count($prototype) > 1) {
                $entity = $this->mapper->createEntity($prototype);
                $aggregate->push($entity);
            }
        }

        $this->cache->set($aggregate);
        return $aggregate;
    }
}