<?php
namespace SFM\Database;

use SFM\Transaction\TransactionException;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Adapter\Driver\StatementInterface;
use Zend\Db\ResultSet\ResultSet;
use SFM\Transaction\TransactionEngineInterface;

class DatabaseProvider implements TransactionEngineInterface
{
    /**
     * @var Adapter
     */
    protected $adapter = null;
    
    /**
     * Current transaction status
     * @var bool
     */
    protected $isTransactionActive = false;

    /**
     * @param AdapterInterface $adapter
     */
    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }
    
    /**
     *  @return Adapter
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * Get query helper
     * @return Query
     */
    public function query()
    {
        return new Query($this->adapter);
    }

    /**
     * @return int
     */
    public function getLastGeneratedValue()
    {
        return $this->adapter->getDriver()->getLastGeneratedValue();
    }

    /**
     * @throws TransactionException
     */
    public function beginTransaction()
    {
        if ($this->isTransactionActive === true) {
            throw new TransactionException("Can't begin transaction while another one is running");
        }

        try {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
            $this->isTransactionActive = true;
        } catch (\Exception $e) {
            throw new TransactionException('Can`t begin transaction', 0, $e);
        }
    }

    /**
     * @return bool
     */
    public function isTransaction()
    {
        return $this->isTransactionActive;
    }
    
    /**
     * @throws TransactionException
     */
    public function commitTransaction()
    {
        if ($this->isTransactionActive === false) {
            throw new TransactionException("Can't commit transaction while no one is running");
        }

        try {
            $this->adapter->getDriver()->getConnection()->commit();
            $this->isTransactionActive = false;
        } catch (\Exception $e) {
            throw new TransactionException('Can`t commit transaction', 0, $e);
        }
    }

    /**
     * @throws TransactionException
     */
    public function rollbackTransaction()
    {
        if ($this->isTransactionActive === false) {
            throw new TransactionException("Can't rollback transaction while no one is running");
        }

        try {
            $this->adapter->getDriver()->getConnection()->rollBack();
            $this->isTransactionActive = false;
        } catch (\Exception $e) {
            throw new TransactionException("Can`t rollback transaction", 0, $e);
        }
    }
}
