<?php
namespace SFM\Database;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\Driver\StatementInterface;

class Query
{
    protected $adapter;

    /**
     * @param Adapter $adapter
     */
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param string $sql
     * @param string[] $params
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function bySql($sql, $params = [])
    {
        return $this->adapter->query($sql, $params);
    }

    /**
     * @param StatementInterface $statement
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function byStatement(StatementInterface $statement)
    {
        return $this->bySql($statement->getSql(), $statement->getParameterContainer()->getNamedArray());
    }
}