<?php
namespace SFM;

/**
 * Abstract class for single business object
 */
abstract class Entity extends Business
{
    /**
     * Contains information about object
     * @var array
     */
    protected $prototype;

    /**
     * @var string
     */
    protected $ifField;

    /**
     * Is not synced with storage
     * @var bool
     */
    protected $dirty = false;

    /**
     * @var Manager
     */
    protected $manager;

    /**
     * @param string[] $prototype Array of data
     * @param string $idField Unique identifier field
     * @param Manager $manager
     */
    public function __construct($prototype, $idField, Manager $manager)
    {
        $this->prototype = $prototype;
        $this->manager = $manager;
        $this->idField = $idField;
        $this->dirty = false;
    }

    /**
     * @param string $field
     * @return string
     */
    public function __get($field)
    {
        return $this->getPrototype($field);
    }

    /**
     * @param string $field
     * @return bool
     */
    public function __isset($field)
    {
        return isset($this->prototype[$field]);
    }
    
    /**
     * Returns the property of Business object if name of the property given, or entire prototype otherwise
     * @param string $field
     * @return string|string[]|null
     */
    public function getPrototype($field = null)
    {
        if ($field === null) {
            return $this->prototype;
        } else if (isset($this->prototype[$field])) {
            return $this->prototype[$field];
        }
         
        return null;
    }

    /**
     * @param string $field
     * @param string $value
     */
    public function setPrototype($field, $value)
    {
        $this->dirty = true;
        $this->prototype[$field] = $value;
    }
    
    /**
     * Return entity id
     * @return int
     */
    public function getId()
    {
        return $this->prototype[$this->idField];
    }

    /**
     * @return string
     */
    public function getIdField()
    {
        return $this->idField;
    }

    /**
     * @return string[]
     */
    public function __sleep()
    {
        return ['prototype', 'idField'];
    }

    /**
     * Is aggregate dirty
     * @return bool
     */
    public function isDirty()
    {
        return $this->dirty;
    }
}