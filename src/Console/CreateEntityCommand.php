<?php
namespace SFM\Console;

use Composer\Script\Event;
use SFM\Console\CreateEntity\MapperScaffold;
use SFM\Console\CreateEntity\EntityScaffold;
use SFM\Console\CreateEntity\AggregateScaffold;
use SFM\Console\CreateEntity\CriteriaScaffold;
use SFM\Console\CreateEntity\QueryBuilderScaffold;
use SFM\Console\CreateEntity\ScaffoldInterface;

class CreateEntityCommand
{
    protected static $path = "src/";

    public static function run(Event $event)
    {
        $tableValidator = function($value) {
            if ('' === trim($value) || preg_match("/^\\w+(?:\\.\\w+)?$/", $value) === 0) {
                throw new \RuntimeException("Valid base class name is required");
            }
            return $value;
        };

        $classValidator = function($value) {
            if ('' === trim($value) || preg_match("/^[a-zA-Z_]+$/", $value) === 0) {
                throw new \RuntimeException("Valid table name is required");
            }
            return $value;
        };

        $fieldValidator = function($value) {
            if ('' === trim($value) || preg_match("/^\\w+(?:\\.\\w+)?$/", $value) === 0) {
                throw new \RuntimeException("Valid id field name is required");
            }
            return $value;
        };

        $table = $event->getIO()->askAndValidate("Specify table name in db: ", $tableValidator);

        $class = str_replace(" ", "_", ucwords(str_replace("_", " ", $table)));
        $class = $event->getIO()->askAndValidate("Specify base class name: ", $classValidator, null, $class);

        $idField = $event->getIO()->askAndValidate("Specify id field name: ", $fieldValidator);

        $scaffolds = array(
            new MapperScaffold($table, $class, $idField),
            new EntityScaffold($table, $class, $idField),
            new AggregateScaffold($table, $class, $idField)
        );

        /** @var $scaffold ScaffoldInterface */
        foreach ($scaffolds as $scaffold) {
            $file = $scaffold->getFilename();
            $info = pathinfo($file);

            if (file_exists(self::$path . $file)) {
                $event->getIO()->write("<info>{$scaffold->getType()} `{$scaffold->getClass()}` already exists.</info>");
                continue;
            }

            if (false === is_dir(self::$path . $info['dirname'])) {
                mkdir(self::$path . $info['dirname'], 0775, true);
            }

            $result = file_put_contents(self::$path . $file, $scaffold->getScaffold());
            if (false === $result) {
                throw new \RuntimeException("Failed creation of `{$file}`");
            }

            $event->getIO()->write("<info>{$scaffold->getType()} `{$scaffold->getClass()}` successfuly created.</info>");
        }
    }
}