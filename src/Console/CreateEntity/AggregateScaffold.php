<?php
namespace SFM\Console\CreateEntity;

class AggregateScaffold extends ScaffoldAbstract
{
    public function getScaffold()
    {
        $scaffold = <<<EOD
<?php
/**
 * @method {$this->entityClass} current() current()
 * @method {$this->entityClass} rewind() rewind()
 * @method {$this->entityClass} next() next()
 * @method {$this->entityClass}[] getEntities() getContent()
 * @method {$this->entityClass} getEntityById() getEntityById(\$id)
 */
class {$this->aggregateClass} extends \SFM\Aggregate
{
}
EOD;

        return $scaffold;
    }

    public function getType()
    {
        return 'Aggregate';
    }
}