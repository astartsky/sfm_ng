<?php
namespace SFM\Console\CreateEntity;

class MapperScaffold extends ScaffoldAbstract
{
    /**
     * @return string
     */
    public function getScaffold()
    {
        $scaffold = <<<EOD
<?php
/**
 * @method {$this->entityClass} getEntityById() getEntityById(int \$id)
 */
class {$this->mapperClass} extends \SFM\Mapper
{
    /**
     * @return string
     */
    public function getTableName()
    {
        return '{$this->table}';
    }

    /**
     * @return string
     */
    public function getIdField()
    {
        return '{$this->idField}';
    }
}
EOD;

        return $scaffold;
    }

    public function getType()
    {
        return 'Mapper';
    }
}