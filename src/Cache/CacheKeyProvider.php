<?php
namespace SFM\Cache;

use SFM\Business;
use SFM\Entity;
use SFM\Manager;
use SFM\Cache\CacheKeyStrategy\CacheKeyStrategyInterface;

class CacheKeyProvider
{
    /**
     * @var CacheKeyStrategyInterface[]
     */
    protected $strategies = [];

    /**
     * @param CacheKeyStrategyInterface $strategy
     */
    public function registerStrategy(CacheKeyStrategyInterface $strategy)
    {
        $this->strategies[] = $strategy;
    }

    /**
     * @param Business $business
     * @return string
     */
    public function getCacheKey(Business $business)
    {
        $cacheKey = null;

        foreach ($this->strategies as $strategy) {
            if ($strategy->isValid($business)) {
                $cacheKey = $strategy->getCacheKey($business);
                break;
            }
        }

        return $cacheKey;
    }

    /**
     * @param Business $business
     * @return string[]
     */
    public function getCacheTags(Business $business)
    {
        $cacheTags = null;

        foreach ($this->strategies as $strategy) {
            if ($strategy->isValid($business)) {
                $cacheTags = $strategy->getCacheTags($business);
                break;
            }
        }

        return $cacheTags;
    }
}