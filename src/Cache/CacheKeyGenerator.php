<?php
namespace SFM\Cache;

use SFM\Entity;
use SFM\BaseException;

class CacheKeyGenerator
{
    /**
     * @param int $id
     * @param string $className
     * @return string
     */
    public function getEntityCacheKeyById($id, $className)
    {
        return $className . CacheProvider::KEY_DELIMITER . $id;
    }

    /**
     * @param int[] $ids
     * @param string $entityClass
     * @return string[]
     */
    public function getCacheKeysByEntitiesId(array $ids, $entityClass)
    {
        $result = array();
        foreach ( $ids as $item ) {
            $result[] = $this->getEntityCacheKeyById($item, $entityClass);
        }
        return $result;
    }

    /**
     * @param Entity $entity
     * @param array $uniqueKey One of the keys. It must contain only field names
     * @return string
     * @throws BaseException
     */
    public function getEntityCacheKeyByUniqueFields(Entity $entity, array $uniqueKey, $entityClass)
    {
        $uniqueVals = array();

        foreach ($uniqueKey as $uniqueKeyItem) {
            if(!is_array($uniqueKeyItem))
                $uniqueKeyItem = array($uniqueKeyItem);
            foreach ($uniqueKeyItem as $item) {
                $val = $entity->getPrototype($item);
                if( null !== $val ) {
                    if(is_string($val)) {
                        $val = mb_strtolower($val);
                    }
                    $uniqueVals[] = $val;
                } else {
                    throw new BaseException('Unknown field - '.$item);
                }
            }
        }
        return $this->getEntityCacheKeyByUniqueVals($uniqueVals, $entityClass);
    }












    public function getEntityCacheKeyByUniqueVals(array $values, $className)
    {
        $key = $className . CacheProvider::KEY_DELIMITER;
        foreach ($values as $item) {
            if(is_string($item)) {
                $item = mb_strtolower($item);
            }
            $key .= CacheProvider::KEY_DELIMITER . $item;
        }
        return $key;
    }
} 