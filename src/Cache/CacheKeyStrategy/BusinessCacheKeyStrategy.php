<?php
namespace SFM\Cache\CacheKeyStrategy;

use SFM\Business;

class BusinessCacheKeyStrategy implements CacheKeyStrategyInterface
{
    /**
     * @param Business $object
     * @return string[]
     */
    public function getCacheKey($object)
    {
        return $object->getCacheKey();
    }

    /**
     * @param Business $object
     * @return string[]
     */
    public function getCacheTags($object)
    {
        return $object->getCacheTags();
    }

    /**
     * @param Business $object
     * @return bool
     */
    public function isValid($object)
    {
        return $object instanceof Business;
    }
}