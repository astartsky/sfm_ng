<?php
namespace SFM\Cache\CacheKeyStrategy;

use SFM\Cache\CacheProvider;
use SFM\Entity;

class EntityCacheKeyStrategy implements CacheKeyStrategyInterface
{
    /**
     * @param Entity $object
     * @return string
     */
    public function getCacheKey($object)
    {
        return get_class($object) . CacheProvider::KEY_DELIMITER . $object->getId();
    }

    /**
     * @param Entity $object
     * @return string[]
     */
    public function getCacheTags($object)
    {
        return [$this->getCacheKey($object)];
    }

    /**
     * @param Entity $object
     * @return bool
     */
    public function isValid($object)
    {
        return $object instanceof Entity;
    }
}