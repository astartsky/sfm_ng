<?php
namespace SFM\Cache\CacheKeyStrategy;

use SFM\Aggregate;
use SFM\Cache\CacheKeyGenerator;
use SFM\Manager;

class AggregateCacheKeyStrategy implements CacheKeyStrategyInterface
{
    protected $entityCacheKeyStrategy;

    /**
     * @param EntityCacheKeyStrategy $entityCacheKeyStrategy
     */
    public function __construct(EntityCacheKeyStrategy $entityCacheKeyStrategy)
    {
        $this->entityCacheKeyStrategy = $entityCacheKeyStrategy;
    }

    /**
     * @param Aggregate $object
     * @return string
     */
    public function getCacheKey($object)
    {
        return $object->getCacheKey();
    }

    /**
     * @param Aggregate $object
     * @return string[]
     */
    public function getCacheTags($object)
    {
        $tags = array();

        if ($object->isLoaded()) {
            foreach ($object as $entity) {
                $tags = array_merge($tags, $this->entityCacheKeyStrategy->getCacheTags($entity));
            }
        } else {
            $tags = (new CacheKeyGenerator())->getCacheKeysByEntitiesId($object->getEntityIds(), get_class($object));
        }

        return $tags;
    }

    /**
     * @param Aggregate $object
     * @return bool
     */
    public function isValid($object)
    {
        return $object instanceof Aggregate;
    }
}