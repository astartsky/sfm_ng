<?php
namespace SFM\Cache\CacheKeyStrategy;

interface CacheKeyStrategyInterface
{
    /**
     * @param $object
     * @return string
     */
    public function getCacheKey($object);

    /**
     * @param $object
     * @return string[]
     */
    public function getCacheTags($object);

    /**
     * @param $object
     * @return string
     */
    public function isValid($object);
}